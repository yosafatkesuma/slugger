<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Cart;

class cartController extends Controller
{
    public function store(Request $request){
        //dd($request);
        $check = $request->cek;
        $kode = $request->kode;
        $nama = $request->nama;
        $harga = $request->harga;
        $jumlah = $request->jumlah;
        $arr = [];
        for ($i = 0; $i < count($check); $i++){
            if($check[$i] == "off"){
                array_push($arr,"off");
            }
            else{
                array_push($arr,"on");
                if($i != count($check)){
                    $i++;
                }
            }
        }
        for($i = 0; $i<count($nama);$i++){
            if($arr[$i] == "on"){
                $c = new Cart;
                $c->Kode = $kode[$i];
                $c->Nama = $nama[$i];
                $c->Harga = $harga[$i];
                $c->jumlah = $jumlah[$i];
                $c->created_by = Auth::user()->name;
                $c->save();
            }
        }
        return redirect()->back();
    }
}
